﻿import sqlalchemy
from sqlalchemy import *
import ibm_db_sa			#db2接続のためのライブラリをインポート
import hashlib				#ハッシュ化のライブラリをインポート

#ユーザ表へアクセスしログインに必要な情報を取得する
class UserDAO:
	
	#コネクトを取得する(インスタンス)
	def __init__(self):
		#コネクトの取得 インスタンス名：パスワード@IPアドレス:ポート番号/データベース名
		self.db2 = sqlalchemy.create_engine('ibm_db_sa://db2inst1:WorkPenguin41@192.168.56.101:50000/m4k1')	


	#パスワードをハッシュ化するかをチェック(初回ログイン時はハッシュ化しない)
	#基本的にloginCheckのみから呼ばれる
	def hashCheck(self, userId, password):
		sql = "select passChangeDay from users where userId = ?"	#パスワード変更日時を取得
		result = self.db2.execute(sql, (userId))			#入力されたユーザIDを設定
		for item in result:						#取得したデータをitemに代入
			data = item[0]						#取得したデータをdataに代入
		if data != None:						#パスワード変更日時にデータが入っているなら
			m = hashlib.sha1()					#sha1を使用してハッシュ化
			m.update(password)					#パスワードをハッシュ化
			return m.digest()[0:20]					#ハッシュ化したパスワードを返す(データベースのデータ長が20文字なので20文字だけ返す)

		return password							#ハッシュ化していないパスワードを返す


	#入力されたユーザIDとパスワードが合っているかのチェック
	#戻り値
	#成功　：　１
	#失敗　：　０
	def loginCheck(self, userId, password):
		password = self.hashCheck(userId, password)				#パスワードをハッシュ化するかチェックする
		sql = "select count(*) from users where userId = ? and password = ?"	#入力されたパスワードが合っていれば1、間違っていれば0
		result = self.db2.execute(sql, (userId, password))			#入力されたユーザID、パスワードを設定
		for item in result:							#取得したデータをitemに代入
			retData = item[0]						#取得したデータをretDataに代入
		return retData								#retDataを返す

	def getUsers(self):								#ユーザID一覧を取得する
		sql = "select userId from users"					#ユーザIDを取得するsql
		result = self.db2.execute(sql)						#sqlを発行
		retData = []								#戻り値のデータをリストとして宣言
		for item in result:							#取得したデータをitemに代入
			retData.append(item[0])						#取得したデータをretDataに代入
		return retData								#retDataを返す

	def upCardId(self, userId, cardId)						#指定したユーザIDのカードIDを指定されたものに変更する(実質的な挿入)
		sql = "update users set cardId = ? where userId = ?"			#カードIDを変更するsql文
		result = self.db2.execute(sql, (cardId, userId))			#sql発行

	def cardIdToUserId(self, cardId)						#カードIDからユーザIDを取得する
		sql = "select userId from users where cardId = ?"			#カードIDを取得するsql
		result = self.db2.execute(sql, (cardId))				#sqlを発行
		for item in result:							#取得したデータをitemに代入
			retData = item[0]						#取得したデータをretDataに代入
		return retData								#retDataを返す

