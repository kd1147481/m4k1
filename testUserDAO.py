﻿#UserDAOをテストするドライバ
import userDAO		#userDAOをインポート

testClass = userDAO.UserDAO()	#userDAOの作成

ret = testClass.loginCheck("0K01004", "0K01004")	#ユーザID、パスワードを指定してloginCheckを呼び出す

print ret		#帰ってきたデータのチェック
if ret:			
	print "true"
else:
	print "false"

ret = testClass.getUsers()				#getUsersを呼び出す
for list in ret:
	print list					#帰ってきたデータのチェック

testClass.upCardId('0K01004', '1234567891234567')

ret = testClass.cardIdToUserId('1234567891234567')
print ret
